import csv
import os
import operator


def main_menu():

    try:
        print('Selamat Datang', nama, '\n')
        print('1. Input Transaksi Baru ')
        print('2. Tampilkan Daftar Transaksi \n')

        pilih = int(input('Pilihan anda : '))

        if pilih == 1:
            os.system('cls')
            input_transaksi()

        if pilih == 2:
            os.system('cls')
            print_transaksi()

    except Exception as e:
        print('Pilihan anda tidak ada', e)


def input_transaksi():
    print('Input Transaksi Baru \n')
    with open('transaksi.csv', 'a', newline='') as csvfile:
        nama_trans = input('Nama Transaksi  : ')
        jum_trans = int(input('Jumlah Transaksi  : '))
        tang_trans = int(input('Tanggal Transaksi : '))
        writer = csv.writer(csvfile)
        writer.writerow([tang_trans, nama_trans, jum_trans])

    os.system('cls')
    main_menu()


def print_transaksi():
    print('=====================================================')
    print('Laporan Transaksi Pembukuan si', nama, 'bulan', bulan)
    print('=====================================================')
    print('  Tanggal    |   Nama Transaksi    |        Jumlah    ')
    print('-----------------------------------------------------')
    jumlah = 0

    with open('transaksi.csv', 'r') as csvfile:
        reader = csv.reader(csvfile)
        sort = sorted(reader, key=operator.itemgetter(0))
        for row in sort:
            print('     ', row[0], '     |       ', row[1], '        |       ', row[2])
            jumlah = jumlah + int(row[2])
    print('-----------------------------------------------------')
    print('Total                                    : ', jumlah)
    print('----------------------------------------------------- \n')

    try:
        print('Tekan 1 untuk kembali ke menu awal,')
        print('Tekan 2 untuk keluar dari program.')

        pilih = int(input('Pilihan Anda : '))

        if pilih == 1:
            print('Kembali ke menu awal')
            os.system('cls')
            main_menu()

        if pilih == 2:
            print('Exit')
            exit()

    except Exception as e:
        print(e)


if __name__ == '__main__':
    nama = input('Nama      : ')
    bulan = input('Bulan     : ')
    os.system('cls')
    main_menu()
